module Lita
  module Handlers
    class Poke < Handler
      route(/poke (\S*)$/i, :pokemon, command: true, help: {
        "poke <name>" => "find a pokemon by name",
        "poke <id>" => "find a pokemon by ID"
      })

      def pokemon(response)
        name = response.match_data[1]
        pokemon = PokeAPI::Pokemon.find name
        target = response.message.source
        attachment = Lita::Adapters::Slack::Attachment.new(pokemon.name, {
          color: "#3367B0",
          title: "#{pokemon.id}. #{pokemon.name}",
          title_link: "http://bulbapedia.bulbagarden.net/wiki/#{pokemon.name}",
          fields: [
            { title: "Types", value: "Grass/Poison", short: true },
            { title: "Abilities", value: "Overgrowth/Chlorophyll", short: true },
            { title: "Evolution", value: "Evolves to Ivisaur at level 16", short: true },
            { title: "EV Yield", value: "1 SpAtk", short: true }
          ],
          thumb_url: pokemon.thumbnail
        })
        robot.chat_service.send_attachments(target, attachment)
        # response.reply(render_template(:pokemon, pokemon: pokemon))
      rescue PokeAPI::Requester::NotFoundError
        response.reply("not found")
      end

      Lita.register_handler(self)
    end
  end
end
